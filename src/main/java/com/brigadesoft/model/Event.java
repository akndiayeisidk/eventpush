package com.brigadesoft.model;

public class Event {
    private int id;
    private String title;
    private String description;
    private String adresse;
    private String longitude;
    private String latitude;
    private String dateEvent;
    private String status;
    private String pic;
    private Categorie categorie;
    private Customer customer;

    public Event() {
    }

    public Event(int id,
                 String title,
                 String description,
                 String adresse,
                 String longitude,
                 String latitude,
                 String dateEvent,
                 String status,
                 String pic,
                 Categorie categorie,
                 Customer customer
    ) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.adresse = adresse;
        this.longitude = longitude;
        this.latitude = latitude;
        this.dateEvent = dateEvent;
        this.status = status;
        this.pic = pic;
        this.categorie = categorie;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
