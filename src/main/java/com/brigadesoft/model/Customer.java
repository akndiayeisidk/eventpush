package com.brigadesoft.model;

public class Customer {
    private int id;
    private String nom;
    private String prenom;
    private String dob;
    private String telephone;
    private String adresse;
    private String email;
    private String pic;

    public Customer() {
    }

    public Customer(int id,
                    String nom,
                    String prenom,
                    String dob,
                    String telephone,
                    String adresse,
                    String email,
                    String pic
    ) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.dob = dob;
        this.telephone = telephone;
        this.adresse = adresse;
        this.email = email;
        this.pic = pic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
