package com.brigadesoft.repository;

import com.brigadesoft.model.Categorie;

public class CategoryRepository {
    public Categorie[] getAll(){
        return new Categorie[]{
                new Categorie(1,"Cinema"),
                new Categorie(2,"Restaurant"),
                new Categorie(3,"Sport"),
                new Categorie(4,"Bien-Etre"),
        };
    }
}
