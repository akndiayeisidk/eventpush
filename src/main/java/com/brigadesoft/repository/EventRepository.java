package com.brigadesoft.repository;

import com.brigadesoft.model.Categorie;
import com.brigadesoft.model.Customer;
import com.brigadesoft.model.Event;

public class EventRepository {
    public Event[] getAllEventCinema(){
        Categorie categorie= new Categorie(1,"Cinema");
        Customer c1= new Customer(1,"NDIAYE","Abdou Khadre","15/08/1995","762231992","Ouest Foire, Dakar","khadrenjaay@gmail.com","null");
        Customer c2= new Customer(2,"SOW","Awa","19/02/1998","762231992","Ouest Foire, Dakar","null","null");

        return new Event[]{
                new Event(1,"Hunter X Hunter","Gon, 11 ans veut retrouver son père...","Canal Olympia",
                        "9856","3547","15/03/2021","public","null",categorie,c1),
                new Event(2,"Code Gees","Guerre entre Britania et Eleven, Who win ?...","Complexe Ousmane Sembène",
                        "123","567","15/03/2021","public","null",categorie,c2),
                new Event(3,"Attaque des Titans","null","Complexe Ousmane Sembène",
                        "123","567","21/03/2021","public","null",categorie,c1),
        };
    }
}
