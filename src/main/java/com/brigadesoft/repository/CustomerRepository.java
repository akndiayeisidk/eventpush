package com.brigadesoft.repository;

import com.brigadesoft.model.Categorie;
import com.brigadesoft.model.Customer;

public class CustomerRepository {
    public Customer[] getAll(){
        return new Customer[]{
                new Customer(1,"NDIAYE","Abdou Khadre","15/08/1995","762231992","Ouest Foire, Dakar","khadrenjaay@gmail.com","null"),
                new Customer(2,"SOW","Awa","19/02/1998","762231992","Ouest Foire, Dakar","null","null"),
                new Customer(3,"SOW","Salimata","null","762231992","Sacré Coeur, Dakar","null","null"),
                new Customer(4,"NDIAYE","Ibrahima","null","762231992","Pikine, Dakar","null","null"),
        };
    }
}
