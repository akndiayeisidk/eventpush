package com.brigadesoft;

import com.brigadesoft.model.Categorie;
import com.brigadesoft.model.Customer;
import com.brigadesoft.model.Event;
import com.brigadesoft.repository.CategoryRepository;
import com.brigadesoft.repository.CustomerRepository;
import com.brigadesoft.repository.EventRepository;

import java.util.Scanner;

public class HomeMenu {
    public void showWelcomeMenu(){

        boolean arret = false;
        while (!arret){

            System.out.println("Welcome to EventPush Web Panel");
            System.out.println("1-Admin");
            System.out.println("2-Modérateur");
            System.out.println("3-Quitter");
            System.out.println("Veuillez choisir un profil pour continuer:");
            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    showAdminMenu();
                    break;
                case 2:
                    //Menu Modérateur
                    break;

                case 3: arret = true; break;

                default: System.out.println("Entrer 1 ou 2"); break;

            }

        }

    }

    public void showAdminMenu(){
        int choice;
        CategoryRepository categoryRepository = new CategoryRepository();
        CustomerRepository customerRepository = new CustomerRepository();
        EventRepository eventRepository = new EventRepository();

        Categorie[] categories = categoryRepository.getAll();
        Customer[] customers = customerRepository.getAll();

        do{
            System.out.println("ADMIN : Welcome to EventPush");
            System.out.println("1-AFFICHER CATEGORIES");
            System.out.println("2-AFFICHER CLIENTS");
            System.out.println("3-AJOUTER PARTENAIRE");
            System.out.println("4-AJOUTER MODERATEUR");
            System.out.println("5-Quitter");
            System.out.println("Veuillez choisir une option pour continuer:");
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("LISTE CATEGORIES");
                    for (int i=0;i<categories.length;i++){
                        Categorie categorie = categories[i];
                        System.out.println(String.format("> %S %S", categorie.getId(),categorie.getLibelle()));
                    }
                    System.out.println("Choissisez une catégorie pour voir les évènements:");
                    int idCategorie = scanner.nextInt();
                    if(idCategorie == 1){
                        System.out.println("LISTE EVENMENTS DE CINEMA:");
                        Event[] events = eventRepository.getAllEventCinema();
                        for (int i=0;i<events.length;i++){
                            Event event = events[i];
                            System.out.println(String.format("> %S %S %S", event.getTitle(),event.getCategorie().getLibelle(),event.getDescription()));
                        }
                    }
                    break;
                case 2:
                    //
                    break;
                case 3:
                    //
                    break;

                default: System.out.println("entrez un choix entre 1 et 5"); break;

            }

        }while(choice!=5);

    }
}
